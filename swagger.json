{
  "swagger": "2.0",
  "info": {
    "title": "Cleardata API",
    "description": "Unlock the Hidden Power of Your Platform. Give your users the best tools in the repossession industry by integrating your application with Cleardata. Stop worrying about user experience and complex infrastructure. Let Cleardata handle it. <br><br>To generate client side code  OR consume this service, please visit <a target=\"__blank\" href=\"http://editor.swagger.io\">Swagger Editor</a>, click File -> Import URL and copy <span> <a href='https://s3-us-west-2.amazonaws.com/dev.cleardata.io/swagger.json'>this link</a></span> and click Import. <br><br>\n Cleardata will use your api key to save your data to our internal companyId for you.",
    "version": "1.0.0"
  },
  "host": "api.cleardata.io/ir",
  "schemes": [
    "https"
  ],
  "basePath": "/v1",
  "securityDefinitions": {
    "apiKeyHeader": {
      "type": "apiKey",
      "name": "Cleardata-APIKey",
      "in": "header"
    }
  },
  "produces": [
    "application/json"
  ],
  "paths": {
    "/import/account": {
      "post": {
        "summary": "Add/Import an Account in Cleardata",
        "description": "The account endpoint adds a new account in Cleardata for reconciliation with Clearplan data. It accepts a single account object. This request is asynchronous - hence the 202. The request is a valid request as long as vin and systemId are sent with at least one address.<br>\nThe systemId is your internal systemId/stock number/unique identifier/etc.<br>\nAddresses Array - An array of addresses, where every address should be passed at least with a street name (name) OR valid lat/lng.<br>\nCustom Data Object - Any data that you would like to be displayed in a cleardata perspective. Properties in this object can only be a string or numeric for now. More types will be available in the future.",
        "security": [
          {
            "apiKeyHeader": []
          }
        ],
        "parameters": [
          {
            "name": "account",
            "in": "body",
            "description": "Account Object",
            "required": true,
            "schema": {
              "$ref": "#/definitions/account"
            }
          }
        ],
        "tags": [
          "Accounts"
        ],
        "responses": {
          "202": {
            "description": "Queued Request"
          },
          "default": {
            "description": "Unexpected error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/import/accounts": {
      "post": {
        "summary": "Add/Import multiple Accounts in Cleardata - batches of up to 50",
        "description": "The accounts endpoint adds multiple new accounts in Cleardata for reconciliation with Clearplan data. It accepts an array of account objects (same as above route). Please refer to the model schema in /import/account for more details.\n\nThis request is asynchronous - hence the 202. The request is a valid request as long as vin and systemId are sent with at least one address.<br>\nThe systemId is your internal systemId/stock number/unique identifier/etc.<br>\nAddresses Array - An array of addresses, where every address should be passed at least with a street name (name) OR valid lat/lng.<br>\nCustom Data Object - Any data that you would like to be displayed in a cleardata perspective. Properties in this object can only be a string or numeric for now. More types will be available in the future.",
        "security": [
          {
            "apiKeyHeader": []
          }
        ],
        "parameters": [
          {
            "name": "account",
            "in": "body",
            "type": "array",
            "description": "Array of Account Objects",
            "required": true,
            "items": {
              "$ref": "#/definitions/account"
            }
          }
        ],
        "tags": [
          "Accounts"
        ],
        "responses": {
          "202": {
            "description": "Queued Request"
          },
          "default": {
            "description": "Unexpected error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/update/account/{systemId}/{status}": {
      "post": {
        "summary": "Update the status of an account with your systemId",
        "description": "This endpoint updates an account status in Cleardata to remove it from the buckets.\n",
        "security": [
          {
            "apiKeyHeader": []
          }
        ],
        "parameters": [
          {
            "name": "systemId",
            "in": "path",
            "description": "You internal system Id for the account.",
            "type": "string",
            "required": true
          },
          {
            "name": "status",
            "in": "path",
            "description": "0 = active, 1 = closed, 2 = repossessed, 3 = resolved, 4 = declined, 5 = charged-off",
            "type": "integer",
            "required": true
          }
        ],
        "tags": [
          "Accounts"
        ],
        "responses": {
          "202": {
            "description": "Queued Request"
          },
          "default": {
            "description": "Unexpected error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    },
    "/account": {
      "post": {
        "summary": "Retrieve Cleardata Processed Vins",
        "description": "Post a set of vins or systemIds to this route and retrieve an array of processed Cleardata information. You may pass in optional params startDate and/or endDate.  If both are given, only accounts with actions that have occurred between the start and end dates provided will be returned. If startDate is given, any accounts with actions that have occurred after the startDate will be returned. If endDate is given, any accounts with actions that have occurred before the endDate will be returned.",
        "security": [
          {
            "apiKeyHeader": []
          }
        ],
        "parameters": [
          {
            "name": "requests",
            "in": "body",
            "description": "Processed Account Objects",
            "schema": {
              "$ref": "#/definitions/accountRequest"
            }
          },
          {
            "name": "startDate",
            "in": "query",
            "description": "Will filter accounts that have had actions occur since the date and time of this parameter - RFC3339 Format UTC (i.e. 2017-12-31T00:00:00Z - do not enclose in quotes). Can be used with or without endDate range.",
            "required": false,
            "type": "string",
            "format": "date-time"
          },
          {
            "name": "endDate",
            "in": "query",
            "description": "Will filter accounts that have had actions occur up to the date and time of this parameter - RFC3339 Format UTC (i.e. 2017-12-31T00:00:00Z - do not enclose in quotes). Can be used with or without startDate range.",
            "required": false,
            "type": "string",
            "format": "date-time"
          }
        ],
        "tags": [
          "Accounts"
        ],
        "responses": {
          "200": {
            "description": "OK",
            "schema": {
              "$ref": "#/definitions/accountResponse"
            }
          },
          "default": {
            "description": "Unexpected error",
            "schema": {
              "$ref": "#/definitions/error"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "accountRequest": {
      "properties": {
        "vin": {
          "type": "array",
          "description": "1 or more Vins in an array",
          "items": {
            "type": "string"
          }
        },
        "systemId": {
          "description": "1 or more SystemIds in an array",
          "type": "array",
          "items": {
            "type": "string"
          }
        }
      }
    },
    "accountResponse": {
      "properties": {
        "results": {
          "type": "array",
          "description": "0 or more objects in an array based on POST data that you provide",
          "items": {
            "$ref": "#/definitions/accountResultObject"
          }
        }
      }
    },
    "accountResultObject": {
      "properties": {
        "vin": {
          "type": "string",
          "description": "The vin of the account"
        },
        "systemId": {
          "type": "string",
          "description": "Your Internal Id for the account."
        },
        "addresses": {
          "type": "array",
          "description": "0 or more objects based on address data you POSTED, this is your addresses reconciled with cleardata findings",
          "items": {
            "$ref": "#/definitions/addresses"
          }
        },
        "cp_addresses": {
          "type": "array",
          "description": "all Clearplan addresses that were found on this vin",
          "items": {
            "$ref": "#/definitions/cp_address"
          }
        },
        "status": {
          "type": "integer",
          "description": "Cleardata status of the account: 0 = active, 1 = closed, 2 = repossessed, 3 = resolved, 4 = declined, 5 = charged-off "
        },
        "seen_in_clearplan": {
          "type": "boolean",
          "description": "flag for whether or not this vin has been seen in Clearplan"
        },
        "cp_companies_count": {
          "type": "integer",
          "description": "The number of companies in Clearplan that have worked with this vin"
        },
        "cp_checkins_count": {
          "type": "integer",
          "description": "The number of total checkins for all addresses on this account done through Clearplan"
        },
        "cp_addresses_count": {
          "type": "integer",
          "description": "The number of Clearplan addresses found associated with this account"
        },
        "cp_confirms_count": {
          "type": "integer",
          "description": "The number of confirmed known addresses for this account in Clearplan"
        },
        "cp_discounts_count": {
          "type": "integer",
          "description": "How many addresses associated with this account have been discounted in Clearplan"
        },
        "cp_recoveries_count": {
          "type": "integer",
          "description": "How many times this vin has been recovered by a company in Clearplan"
        },
        "anyStringProperty": {
          "type": "string",
          "description": "Some string data, such as 'vehicle_type', that you may have posted for Cleardata to sort and group"
        },
        "anyDateProperty": {
          "type": "string",
          "description": "Some date data formatted to a string, such as 'date_account_received', that you may have posted for Cleardata to sort and group"
        },
        "anyNumericProperty": {
          "type": "integer",
          "description": "Some numeric data that you may have posted for Cleardata to sort and group"
        }
      }
    },
    "addresses": {
      "properties": {
        "id": {
          "type": "string",
          "description": "This is your system's internal address id."
        },
        "street_1": {
          "type": "string",
          "description": "Street Address 1 of this location."
        },
        "street_2": {
          "type": "string",
          "description": "Street Address 2 of this location."
        },
        "city": {
          "type": "string",
          "description": "City the address is in"
        },
        "state": {
          "type": "string",
          "description": "State the address is in"
        },
        "zip": {
          "type": "string",
          "description": "Zip code of address"
        },
        "location": {
          "$ref": "#/definitions/location",
          "description": "lat/lng of the Address you posted."
        },
        "valid": {
          "type": "boolean",
          "description": "Defines if the address is a valid."
        },
        "type": {
          "type": "string",
          "default": "Home",
          "enum": [
            "Home",
            "Work",
            "Day Scan",
            "Night Scan",
            "Camera Scan",
            "Alternate"
          ],
          "description": "Address Type"
        },
        "matched": {
          "type": "boolean",
          "description": "flag to check whether this address had a matching address in clearplan"
        },
        "matched_address": {
          "$ref": "#/definitions/cp_address",
          "description": "the clearplan address that was matched to this address"
        }
      }
    },
    "cp_address": {
      "properties": {
        "street_1": {
          "type": "string",
          "description": "Street Address 1 of this location."
        },
        "street_2": {
          "type": "string",
          "description": "Street Address 2 of this location."
        },
        "city": {
          "type": "string",
          "description": "City the address is in"
        },
        "state": {
          "type": "string",
          "description": "State the address is in"
        },
        "zip": {
          "type": "string",
          "description": "Zip code of address"
        },
        "location": {
          "$ref": "#/definitions/location",
          "description": "lat/lng of the clearplan address"
        },
        "type": {
          "type": "string",
          "default": "Home",
          "enum": [
            "Home",
            "Work",
            "Day Scan",
            "Night Scan",
            "Camera Scan",
            "Alternate"
          ],
          "description": "Address Type"
        },
        "actions": {
          "type": "array",
          "description": "0 or more objects based on how many actions have happened in Clearplan to this address",
          "items": {
            "$ref": "#/definitions/actions"
          }
        }
      }
    },
    "account": {
      "required": [
        "vin",
        "systemId"
      ],
      "properties": {
        "systemId": {
          "type": "string",
          "description": "Your internal system Id for the account."
        },
        "vendorId": {
          "type": "string",
          "description": "Your vendor's id for the account."
        },
        "vendorTypeId": {
          "type": "string",
          "enum": [
            "RDN",
            "iRepo",
            "MBSi",
            "Other"
          ],
          "description": "Your vendor's id for the account."
        },
        "vin": {
          "type": "string",
          "description": "VIN of the vehicle."
        },
        "repo_type": {
          "type": "string",
          "default": "Repossess",
          "enum": [
            "Auction",
            "Cond. Report",
            "Consign",
            "Conv to Repo",
            "Field Visit",
            "Impound Repo",
            "Impound Voluntary",
            "Investigate",
            "Pictures",
            "Repossess",
            "Store",
            "Tow",
            "Transport",
            "Vol Repo.",
            "Investigate/Repo",
            "LPR Staging"
          ],
          "description": "Repossession Type"
        },
        "first_name": {
          "type": "string",
          "description": "Debtor first name"
        },
        "middle_name": {
          "type": "string",
          "description": "Debtor middle name"
        },
        "last_name": {
          "type": "string",
          "description": "Debtor last name"
        },
        "phone_number": {
          "type": "string",
          "description": "Debtor phone number"
        },
        "first_name_2nd": {
          "type": "string",
          "description": "Secondary debtor first name"
        },
        "middle_name_2nd": {
          "type": "string",
          "description": "Secondary debtor middle name"
        },
        "last_name_2nd": {
          "type": "string",
          "description": "Secondary debtor last name"
        },
        "phone_number_2nd": {
          "type": "string",
          "description": "Secondary debtor phone number"
        },
        "year": {
          "type": "string",
          "description": "Year of the vehicle. E.g. 2014"
        },
        "make": {
          "type": "string",
          "description": "Make of the vehicle. E.g. BMW"
        },
        "model": {
          "type": "string",
          "description": "Model of the vehicle. E.g. I8"
        },
        "color": {
          "type": "string",
          "description": "Color of the vehicle. E.g. Grey"
        },
        "plate": {
          "type": "string",
          "description": "Plate of the vehicle."
        },
        "orderDate": {
          "type": "string",
          "description": "Date the account was received from your client in ISO 8601 format. E.g. \"2017-01-31T18:52:45-07:00\""
        },
        "client": {
          "description": "Client object",
          "$ref": "#/definitions/client"
        },
        "lienholder": {
          "description": "Lienholder object",
          "$ref": "#/definitions/lienholder"
        },
        "addresses": {
          "type": "array",
          "description": "Array of Addresses",
          "items": {
            "$ref": "#/definitions/address"
          }
        },
        "custom": {
          "description": "Custom data object",
          "$ref": "#/definitions/custom"
        }
      }
    },
    "client": {
      "properties": {
        "name": {
          "type": "string",
          "description": "Name of the Client."
        },
        "id": {
          "type": "string",
          "description": "Your Internal Id of the Client."
        },
        "followUpDate": {
          "type": "string",
          "description": "follow up due date if applicable"
        }
      }
    },
    "lienholder": {
      "properties": {
        "name": {
          "type": "string",
          "description": "Name of the Lien holder."
        },
        "id": {
          "type": "string",
          "description": "Your Internal Id of the Lien Holder."
        },
        "followUpDate": {
          "type": "string",
          "description": "follow up due date if applicable"
        }
      }
    },
    "location": {
      "properties": {
        "latitude": {
          "type": "number",
          "format": "double",
          "description": "Latitude of the location."
        },
        "longitude": {
          "type": "number",
          "format": "double",
          "description": "Longitude of the location."
        }
      }
    },
    "address": {
      "properties": {
        "id": {
          "type": "string",
          "description": "This is your system's internal address id."
        },
        "street_1": {
          "type": "string",
          "description": "Street Address 1 of this location."
        },
        "street_2": {
          "type": "string",
          "description": "Street Address 2 of this location."
        },
        "city": {
          "type": "string",
          "description": "City the address is in"
        },
        "state": {
          "type": "string",
          "description": "State the address is in"
        },
        "zip": {
          "type": "string",
          "description": "Zip code of address"
        },
        "location": {
          "$ref": "#/definitions/location",
          "description": "Location objection of Address."
        },
        "valid": {
          "type": "boolean",
          "description": "Defines if the address is a valid."
        },
        "type": {
          "type": "string",
          "default": "Home",
          "enum": [
            "Home",
            "Work",
            "Day Scan",
            "Night Scan",
            "Camera Scan",
            "Alternate"
          ],
          "description": "Address Type"
        }
      }
    },
    "actions": {
      "properties": {
        "time": {
          "type": "string",
          "description": "the date and time this action occurred, string formatted 'M/dd/yyyy h:mma' (i.e. 4/12/2017 12:00:00PM)"
        },
        "type": {
          "type": "string",
          "description": "the type of action: Check-in, Discounted, Confirmed, Recovered"
        },
        "company": {
          "type": "string",
          "description": "Which anonymous company performed the event, values will be C1, C2, C3...C(N) based on how many companies have worked with this event"
        }
      }
    },
    "custom": {
      "properties": {
        "anyStringProperty": {
          "type": "string",
          "description": "Some string data, such as 'vehicle_type', that you would like grouped in perspectives, include as many as necessary"
        },
        "anyDateProperty": {
          "type": "string",
          "description": "Some date data formatted to a string, such as 'date_account_received', that you would like grouped in perspectives, include as many as necessary"
        },
        "anyNumericProperty": {
          "type": "integer",
          "description": "Some numeric data that you would like grouped in perspectives, include as many as necessary"
        }
      }
    },
    "error": {
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        },
        "fields": {
          "type": "string"
        }
      }
    }
  }
}